"""
Docker examples from: https://docs.docker.com/develop/sdk/examples/ and ...
If the docker module has not yet been installed:
import pip
pip.main(['install', 'docker'])
"""
import datetime, os, os.path, pexpect, tarfile
import docker

def mktar(path=None, tarname='/var/tmp/docker-import.tar'):
  f = None
  try:
    f = os.mknod(tarname)
    t = tarfile.open(mode='w', fileobj=f)
    abs_path = os.path.abspath(path)
    t.add(abs_path, arcname=os.path.basename(path), recursive=False)
    t.close()
    f.seek(0)
  except:
    print 'failed to create tar: ', tarname

  return f

def untar(tardata, filename):
  with tarfile.open(mode='r', fileobj=tardata) as t:
    f = t.extractfile(filename)
    result = f.read() ; f.close()
    return result

def importtar(client=None, tarname='/var/tmp/docker-import.tar'):
  """
  Docker low-level API Parameters:
    filename (str) – Full path to a tar file.
    repository (str) – The repository to create
    tag (str) – The tag to apply
  Raises:
    IOError – File does not exist.
  """

  # res = pexpect.run('docker imaport '+tarname)
  if( not client ): client = docker.from_env() # docker.APIClient(base_url='unix://var/run/docker.sock')
  try:
    import_image_from_file(tarname) # , repository=None, tag=None, changes=None)
  except IOError as ioe:
    print ioe


client = docker.from_env() # docker.APIClient(base_url='unix://var/run/docker.sock')
client.version()

alpine = client.images.pull("alpine") ; print alpine.id
nginx = client.images.pull('nginx') ; print nginx.id
client.images.list()

"""
Run alpine container in foreground shell and exit after completion of echo
"""
print client.containers.run("alpine", ["echo", "hello", "world"])

"""
Run alpine container in background and create a new image from it
"""
container = client.containers.run("alpine", ["touch", "/helloworld"], detach=True)
container.wait()
image = container.commit("helloworld")
print image.id

"""
Run alpine container in background and print config of all running containers
"""
container = client.containers.run("bfirsh/reticulate-splines", detach=True)
print container.id
for container in client.containers.list():
  print container.id, container.attrs['Config']


"""
Print logs of specific container by id
Print all logs
"""
container = client.containers.get('f1064a8a4c82')
print container.id, container.logs()
for line in container.logs(stream=True):
  print line.strip()



"""
Pause or stop a running container
"""
for container in client.containers.list():
  print container.id, container.pause() # container.stop()
