#!/usr/bin/env python

from __future__ import print_function

import os, string, sys
if sys.version_info.major < 3: import six # allows python3 syntax in python2

import datetime, errno, getopt, json, logging, pyexpect, signal, socket, time

def iptables(savefilename='docker_ipables'):
  """
  Iptables rules for Docker container port mapping with openVswitch bridge.
  Use pyexpect module for iptables rules insertion and iptables-save
  """

  return None
#end iptables

def docker(containers={'registy': '172.17.0.5:5000', 'portainer/portainer': '172.17.0.9:9000', 'gitlab/gitlab-ce': '172.17.0.90:22:80:443'}):
  """
  Docker run CLI via pyexpect
  Pexpect.run() runs the given command; waits for it to finish; then returns all stdout and stderr combined as one string.
  """
  dps = pexpect.run('docker ps')
  print('docker> all active containers:\n'+dps)

  # docker run -dit -p ${pubIP}:5000:5000 --net=none --name=registry registry
  hostname = socket.getfqdn()
  pubIP = socket.gethostbyname(hostname)
  dports = '-p ' + pubIP + ':5000:5000 '
  dname = ' --name registry registry '
  dcli = 'docker run -dit --net=none ' + dports + dname
  print('docker> run container via:\n'+dcli)
  drun = pexpect.run(dcli)
  print('docker> newly active container:\n'+drun)
  dps = pexpect.run('docker ps')
  print('docker> all active containers:\n'+dps)

  # docker run -dit -p ${pubIP}:9000:9000 -v "/var/run/docker.sock:/var/run/docker.sock" --privileged --name=portainer --net=none portainer/portainer
  # docker run -dit -p ${pubIP}:9090:9090 --privileged --name=cockpit --net=none cockpit/ws
  # docker run -dit -p ${pubIP}:9022:22 -p ${pubIP}:9080:80 -p ${pubIP}:9443:443 --privileged --name=gitlab --net=none gitlab/gitlab-ce

  print(dps)
  return None
#end docker

def ovs(ovsbr='ovs-cloudbr0', ipgw='172.17.0.1', containers={'registy': '172.17.0.10:5000'}):
  """
  OpenVswitch ovs-vsctl and ovs-docker CLI via pyexpect
  """

  return None
#end ovs

def catalog(port=5000):
  """
  Return list of all container images available from the local Docker Registry.
  Equiv. to cURL http://localhost:port/v2/_catalog
  """
  port = str(port)
#end catalog

def snapshotTar(name='tomcat8', tarball='container-image-name.tar.gz'):
  """
  Figure out the active container Id from the given name then use docker commit CLI
  to pause the container and create a snapshot. Then use the docer save CLI to
  create a tarfile of the snapshot and compress it.
  The compressed tarfile / tarball can later be spun-up via docker load.
  Note that a docker image tarball created via supermin(5) lacks some metadata
  and one must use the CLI docker import (not load) to run such images initially.
  """
  print('snapshotTar> pausing container '+id+' and creating snapshot '+tarball)
  print('snapshotTar> this can take awhile ... so if you wanna abort, hit control-c') # need sighandler

  id = pexpect.run("docker ps -a | grep " + name + " | awk '{print $1}'")
  snap = name+'_snapshot'
  dcli = 'docker commit -p' + id + ' ' + snap
  dret = pexpect.run(dcli)

  dcli = 'docker save -o ' + tarball + ' ' + snap
  dret = pexpect.run(dcli)

  print('snapshotTar> to restore / boot this snapshot: docker load -i ' + tarball)
#end snapshotTar

def registerImg(name='tomocat8', image='tomcat:8', regport=5000):
  """
  Tag the conainer image tarfile and push it to the local registry.
  Returns the current catalog of local registry images.
  Use docker import CLI to run the image wherever.
  """
  print('registerImg> tag the active container and push it to local registry')
  dcli = pexpect.run('docker tag '+name+'localhost:5000/'+image)
  dcli += pexpect('docker push localhost:5000/'+image)
  print('registerImg> ', dcli)

  cat = catalog() # print(cat)
  return cat
#end registerImg

def usage():
  cli = 'ovsdocker.py [-h --help] [-v] [-c] [-t [iptables-filename]] [-r register-image] --iptables=filename --register=tarball --ipgw=172.17.0.1 --ipcon=172.17.0.10 --ipbr=routeable-public-IP --hostport=9080 --conport=80 --ovsbr=cloudbr0 container-name'
  print(cli)
  cli = '-h or --help prints usage'
  print(cli)
  cli = '-v for verbose log'
  print(cli)
  cli = '-c to fetch and print local registry catalog'
  print(cli)
  cli = '-t to print iptables rules and optionaly save to filename'
  print(cli)
  cli = '--ovsbr= name of OVS bridge to setup for use by container'
  print(cli)
  cli = '--ipcon= private IP assigned to container'
  print(cli)
  cli = '--ipgw= gateway IP assigned to container should be same subnet as container private IP'
  print(cli)
  cli = '--ipbr= publicly routable IP assigned to OpenVswitch (OVS) bridge'
  print(cli)
  cli = '--hostport= host port to map to container port'
  print(cli)
  cli = '--conport= container port to map from host port'
  print(cli)
  cli = 'container-name= container name should be related to image-name'
  print(cli)
#end usage

if __name__ == '__main__':
  boolopts = 'chv'
  valopts = 'r:t:'
  keyvalopts = ['help','iptable=','register=','ipcon=','ipbr=','ipgw=','hostport=','conport=','ovsbr=']
  try:
    opts, args = getopt.getopt(sys.argv[1:],boolopts+valopts, keyvalopts)
  except Exception as e:
    print(str(e))
    sys.exit(1)

#  if('--ippub' in opts):
#    pubIP = str(opts['--ippub'])

#end __main__
