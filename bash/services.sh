#!/bin/sh
systemctl start openvswitch
systemctl start iptables
systemctl start libvirtd
systemctl start docker

# after starting above:
#-P INPUT ACCEPT
#-P FORWARD ACCEPT
#-P OUTPUT ACCEPT
#-N DOCKER
#-N DOCKER-ISOLATION
#-A INPUT -i virbr0 -p udp -m udp --dport 53 -j ACCEPT
#-A INPUT -i virbr0 -p tcp -m tcp --dport 53 -j ACCEPT
#-A INPUT -i virbr0 -p udp -m udp --dport 67 -j ACCEPT
#-A INPUT -i virbr0 -p tcp -m tcp --dport 67 -j ACCEPT
#-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
#-A INPUT -p icmp -j ACCEPT
#-A INPUT -i lo -j ACCEPT
#-A INPUT -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT
#-A INPUT -j REJECT --reject-with icmp-host-prohibited
#-A FORWARD -d 192.168.122.0/24 -o virbr0 -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
#-A FORWARD -s 192.168.122.0/24 -i virbr0 -j ACCEPT
#-A FORWARD -i virbr0 -o virbr0 -j ACCEPT
#-A FORWARD -o virbr0 -j REJECT --reject-with icmp-port-unreachable
#-A FORWARD -i virbr0 -j REJECT --reject-with icmp-port-unreachable
#-A FORWARD -j REJECT --reject-with icmp-host-prohibited
#-A OUTPUT -o virbr0 -p udp -m udp --dport 68 -j ACCEPT
#-A DOCKER-ISOLATION -j RETURN
