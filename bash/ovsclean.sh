#!/bin/sh
# http://www.brocade.com/content/html/en/configuration-guide/SDN-Controller-2.1.0-OpenStack-Integration/GUID-492F8FB0-0287-45EC-8570-3628D2CDBE60.html
# Cleaning up compute nodes and resetting the Open vSwitch
# This section describes how to clean up the compute nodes and reset the Open vSwitch.
# Use the following script to stop the Open vSwitch agent.
systemctl stop neutron-openvswitch-agent
systemctl disable neutron-openvswitch-agent

# Stops, cleans and restarts Open vSwitch. Log files are captured.
# Enter the following commands to reset the Open vSwitch.
systemctl stop openvswitch
rm -rf /var/log/openvswitch/*
rm -rf /etc/openvswitch/conf.db
systemctl start openvswitch
ovs-vsctl show
