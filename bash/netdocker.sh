#!/bin/sh
docker network ls
docker network inspect none
docker network inspect host
docker network inspect bridge

# overlay network requires a swarm init ...
docker swarm init
#docker network create --driver overlay --subnet 172.17.0.0/16 --opt encrypted netoverlay
docker network create --driver overlay --subnet 172.17.0.0/16 netoverlay

docker network ls
docker network inspect netoverlay

