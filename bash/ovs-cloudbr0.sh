#!/bin/sh
# http://cloudgeekz.com/400/how-to-use-openvswitch-with-docker.html

# clean slate

#docker swarm init
docker network ls
docker swarm leave --force
echo sleep 5 sec to ensure all swarm bridge endpoints are removed
for s in {1..5} ; do
  echo sleeping ...
  sleep 1
done
brctl show

docker network rm docker_gwbridge
docker network rm docker0

systemctl stop docker
ip link delete dev docker0
ip link delete dev docker_gwbridge
systemctl start docker
docker network ls

systemctl retstart openvswitch

ovs-vsctl del-br ovs-cloudbr0
ovs-vsctl add-br ovs-cloudbr0
ip addr add dev ovs-cloudbr0 172.17.0.1/16
ip link set dev ovs-cloudbr0 up
ip link add veth_hostc0 type veth peer name veth_contc0
ovs-vsctl add-port ovs-cloudbr0 veth_hostc0

name=gitlab
image=gitlab/gitlab-ce
docker rm -f $name
docker run -itd -p 9443:443 --net='none' -itd --name $name $image /bin/bash
docker exec -it $name bash -c "ip addr"

id=`docker ps | grep $name`
dpid=`docker inspect -f '{{.State.Pid}}' $id`

mkdir -p /var/run/netns
ln -s /proc/${dpid}/ns/net /var/run/netns/${dpid}

ip netns exec $dpid ip link set dev veth_contc0 name eth0
ip netns exec $dpid ip link set dev eth0 up

docker exec -it $name bash -c "ip addr"

# assign an IP to the new renamed eth0 in the container
docker exec -it $name bash -c "ip addr add dev eth0 172.17.0.2/16"

\ping -c 1 172.17.0.1
\ping -c 1 172.17.0.2

