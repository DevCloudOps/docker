#!/bin/sh
# http://docs.openvswitch.org/en/latest/howto/kvm/

cat << 'EOF' > /etc/ovs-ifup
#!/bin/sh
switch='kvmbr0'
/sbin/ifconfig $1 0.0.0.0 up
ovs-vsctl add-port ${switch} $1
EOF

cat << 'EOF' > /etc/ovs-ifdown
#!/bin/sh
switch='kvmbr0'
/sbin/ifconfig $1 0.0.0.0 down
ovs-vsctl del-port ${switch} $1
EOF

ovs-vsctl add-br kvmbr0
ovs-vsctl add-port kvmbr0 eth0

echo start a guest that will use our ifup and ifdown scripts:
echo kvm -m 512 -net nic,macaddr=00:11:22:EE:EE:EE -net tap,script=/etc/ovs-ifup,downscript=/etc/ovs-ifdown -drive file=/path/to/disk-image,boot=on

ovs-dpctl show
ovs-ofctl show kvmbr0
