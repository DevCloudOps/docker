#!/bin/sh

os=`uname`
if [[ $os != Linux ]] ; then
  echo sorry ... this bash script only works on Linux ... not $os
  exit
fi

# this iptables error implies something is amiss with the docker bridge ...
# sudo docker run -d -v /data/ribbon:/data -p 80:80 --name data-ribbon ribbon-php-python
# 016150508c48ee99dd910a3040040e2e045c54a3a240773ec747170d12de287e
# Error response from daemon: Cannot start container 016150508c48ee99dd910a3040040e2e045c54a3a240773ec747170d12de287e: 
# iptables failed: iptables -t nat -A DOCKER -p tcp -d 0/0 --dport 80 -j DNAT --to-destination 172.17.0.6:80 ! -i docker0:
# iptables: No chain/target/match by that name.

function brdocker0 {
  if [ $1 ] ; then 
    echo 'if docker0 bridge exists shut it down and delete it ...'
  fi
  d0=`brctl show docker0 | grep -i no`
  if [ $? != 0 ] ; then
    echo no docker0 bridge ... $d0
    return
  fi
  ip link set docker0 down
  brctl delbr docker0
}

if [ $1 ] ; then 
  brctl show
fi

echo shutdown docker daemon and delete its bridge, then restart the daemon ..
service docker stop

brdocker0 -v

service docker start

