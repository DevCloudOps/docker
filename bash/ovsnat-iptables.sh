#!/bin/sh
# https://medium.com/@joatmon08/making-it-easier-docker-containers-on-open-vswitch-4ed757619af9

pubnic=enp5s0f1 # enp5s0f0
bridge=ovs-cloudbr0
iptables -t nat -A POSTROUTING -o $pubnic -j MASQUERADE
iptables -A FORWARD -i $bridge -j ACCEPT
iptables -A FORWARD -i $bridge -o $pubnic -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -S

#-P INPUT ACCEPT
#-P FORWARD ACCEPT
#-P OUTPUT ACCEPT
#-N DOCKER
#-N DOCKER-ISOLATION
#-A FORWARD -j DOCKER-ISOLATION
#-A FORWARD -o docker0 -j DOCKER
#-A FORWARD -o docker0 -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
#-A FORWARD -i docker0 ! -o docker0 -j ACCEPT
#-A FORWARD -i docker0 -o docker0 -j ACCEPT
#-A FORWARD -i ovs-br1 -j ACCEPT
#-A FORWARD -i ovs-br1 -o eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT
#-A DOCKER-ISOLATION -j RETURN
